<!DOCTYPE html>
<html lang="en">
    <head>
        <title>valobashar Diary</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/custom.css">
        <script src="js/bootstrap.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <style>
        .center{
            text-align: center;
        }
    </style>
    <body>
        <div class="container">
            <div class="jumbotron1">
                <div class="jumbotrone">
                    <form action="home.php" method="post">
                        <div >
                            <span><h2 class="d-loginheader">User Login</h2></span>
                        </div>
                        <div >
                            <h4><small>Please Fill up with Login Credentials</small></h4>
                        </div>
                        <div class="form-group">
                            <label for="email">Email / username:<strong class="text-danger" style="font-size: 18px;">*</strong></label>
                            <input type="email" class="form-control" id="email" placeholder="Username or E-amil">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Password:<strong class="text-danger" style="font-size: 18px;">*</strong></label>
                            <input type="password" class="form-control" id="pwd" placeholder="Password">
                        </div>
                        <button type="submit" class="btn btn-default"><abb title="if you have a account" >Login</abb></button>
                        <button type="submit" class="btn btn-default"><abb title="Click here to Create a new account.." >Sign up</abb></button>
                        <h3><abbr title="Fields with * are required."><small>Condition</small></abbr></h3>
                        <span >© 2016 valobashardiary.com . All Rights Reserved.</span>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
